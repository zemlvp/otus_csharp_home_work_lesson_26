﻿using System;
using System.Collections.Generic;
using System.Text;

namespace otus_csharp_home_work_lesson_26
{
    public class ExportJSON : IExport
    {
        public string Export(Point point)
        {
            return
$@"
{{""X"": {point.x}, ""Y"": {point.y}}}
";
        }
    }
}
