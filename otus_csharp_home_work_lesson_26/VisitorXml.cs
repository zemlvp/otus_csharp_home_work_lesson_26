﻿namespace otus_csharp_home_work_lesson_26
{
    public class VisitorXml : IVisitor
    {
        public string Export(Point point)
        {
            return
$@"<POINT>
    <X>{point.x}</X>
    <Y>{point.y}</Y>
</POINT";
        }

        public string Export(Circle circle)
        {
            return
$@"<CIRCLE>
    <X>{circle.x}</X>
    <Y>{circle.y}</Y>
    <R>{circle.radius}</R>
</CIRCLE";
        }

        public string Export(Square square)
        {
            return
$@"<SQUARE>
    <X>{square.x}</X>
    <Y>{square.y}</Y>
    <SIDE>{square.side}</SIDE>
</SQUARE";
        }
    }
}
