﻿namespace otus_csharp_home_work_lesson_26
{
    public class VisitorJson : IVisitor
    {
        public string Export(Point point)
        {
            return $@"{{""X"": {point.x}, ""Y"": {point.y}}}";
        }

        public string Export(Circle circle)
        {
            // Копипаста. Радиус отображается как второй Y
            return $@"{{""X"": {circle.x}, ""Y"": {circle.y}, ""Y"": {circle.radius}}}";
        }

        public string Export(Square square)
        {
            // Копипаста. Сторона квадрата отображается как второй Y
            return $@"{{""X"": {square.x}, ""Y"": {square.y}, ""Y"": {square.side}}}";
        }
    }
}
