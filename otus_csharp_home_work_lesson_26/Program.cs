﻿using System;

namespace otus_csharp_home_work_lesson_26
{
    class Program
    {
        static void Main()
        {
            // Паттерн стратегия
            Console.WriteLine("\nПаттерн стратегия");
            Point point = new Point(10, 10);
            IExport exportXml = new ExportXML();
            IExport exportJson = new ExportJSON();

            Context context = new Context(exportXml);
            string data = context.DoExport(point);
            Console.WriteLine(data);

            context = new Context(exportJson);
            data = context.DoExport(point);
            Console.WriteLine(data);

            // Паттерн Посетитель
            Console.WriteLine("\nПаттерн Посетитель");
            Circle circle = new Circle(100, 100, 20);
            Square square = new Square(100, 100, 15);

            // xml
            Console.WriteLine("\nxml:");
            IVisitor visitor = new VisitorXml();

            data = visitor.Export(point);
            Console.WriteLine(data);

            data = visitor.Export(circle);
            Console.WriteLine(data);

            data = visitor.Export(square);
            Console.WriteLine(data);

            // json
            Console.WriteLine("\njson:");
            visitor = new VisitorJson();

            data = visitor.Export(point);
            Console.WriteLine(data);

            data = visitor.Export(circle);
            Console.WriteLine(data);

            data = visitor.Export(square);
            Console.WriteLine(data);

            Console.ReadKey();
        }
    }
}
