﻿namespace otus_csharp_home_work_lesson_26
{
    public class ExportXML : IExport
    {
        public string Export(Point point)
        {
            // Имя класса в XML - Point. А какие имя будет,
            // если в метод будет передан дочерний класс?
            return
$@"<POINT>
    <X>{point.x}</X>
    <Y>{point.y}</Y>
</POINT";
        }
    }
}
