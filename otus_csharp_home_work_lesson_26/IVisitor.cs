﻿namespace otus_csharp_home_work_lesson_26
{
    public  interface IVisitor
    {
        string Export(Point point);
        string Export(Circle circle);
        string Export(Square square);
    }
}
