﻿namespace otus_csharp_home_work_lesson_26
{
    interface IExport
    {
        public string Export(Point point);
    }
}