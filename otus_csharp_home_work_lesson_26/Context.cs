﻿namespace otus_csharp_home_work_lesson_26
{
    class Context
    {
        // модификатор readonly необходим, чтобы запретить изменение поля после инициации
        private IExport export;
        public Context(IExport export)
        {
            this.export = export;
        }

        public string DoExport(Point point) {
            return export.Export(point);
        }
    }
}
