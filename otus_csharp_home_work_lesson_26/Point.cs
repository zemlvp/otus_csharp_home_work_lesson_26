﻿namespace otus_csharp_home_work_lesson_26
{
    public class Point
    {
        // Поля открыты для изменений извне после инициализации проекта.
        // лучше переделать их в readonly свойства
        public int x;
        public int y;

        public Point(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public string ExportXML()
        {
            return
$@"<POINT>
    <X>{y}</X>
    <Y>{x}</Y>
</POINT";
        }

        public string ExportJSON()
        {
            // Для имен свойств в строке лучше использовать nameof()
            return
$@"
{{""X"": {x}, ""Y"": {y}}}
";
        }

        public virtual string Export(IVisitor visitor)
            => visitor.Export(this);
    }
}
