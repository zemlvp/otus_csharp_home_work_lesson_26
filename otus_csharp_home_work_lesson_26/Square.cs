﻿namespace otus_csharp_home_work_lesson_26
{
    public class Square : Point
    {
        public int side;

        public Square(int x, int y, int side) : base(x, y)
        {
            this.side = side;
        }

        public override string Export(IVisitor visitor)
            => visitor.Export(this);
    }
}
