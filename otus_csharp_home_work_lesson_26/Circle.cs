﻿namespace otus_csharp_home_work_lesson_26
{
    public class Circle : Point
    {
        public int radius;

        public Circle(int x, int y, int radius) : base(x, y)
        {
            this.radius = radius;
        }

        public override string Export(IVisitor visitor)
            => visitor.Export(this);
    }
}
