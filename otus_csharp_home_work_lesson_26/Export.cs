﻿using System;
using System.Collections.Generic;
using System.Text;

namespace otus_csharp_home_work_lesson_26
{
    // Неиспользоемые файлы/классы лучше удалить
    public class Export
    {
        public string ExportXML(Point point)
        {
            return
$@"<POINT>
    <X>{point.x}</X>
    <Y>{point.y}</Y>
</POINT";
        }

        public string ExportJSON(Point point)
        {
            return
$@"
{{""X"": {point.x}, ""Y"": {point.y}}}
";
        }
    }
}
